
function High1000(web3) {

	var contractAddress = '0x40f4765db693647f592fc796bc249f768ce5b638';
	var contractAbi = [ { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldTarget", "type": "address" }, { "indexed": false, "name": "newTarget", "type": "address" } ], "name": "ContractUpgraded", "type": "event" }, { "constant": false, "inputs": [ { "name": "_name", "type": "string" }, { "name": "_groupId", "type": "uint8" }, { "name": "_country", "type": "string" }, { "name": "_linkedInProfile", "type": "string" }, { "name": "_avatarUrl", "type": "string" } ], "name": "addMember", "outputs": [ { "name": "", "type": "bytes32" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "claimAdminRights", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "deleteMember", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_avatarUrl", "type": "string" } ], "name": "setAvatarUrl", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newManager", "type": "address" } ], "name": "setManager", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_country", "type": "string" } ], "name": "setMemberCountry", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_groupId", "type": "uint8" } ], "name": "setMemberGroup", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "propertyIndex", "type": "uint256" }, { "indexed": false, "name": "newValue", "type": "string" } ], "name": "MemberStringPropertyChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "registrationTime", "type": "uint256" }, { "indexed": false, "name": "name", "type": "string" }, { "indexed": false, "name": "groupId", "type": "uint8" }, { "indexed": false, "name": "country", "type": "string" }, { "indexed": false, "name": "linkedInProfile", "type": "string" }, { "indexed": false, "name": "avatarUrl", "type": "string" } ], "name": "MemberAdded", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldManager", "type": "address" }, { "indexed": false, "name": "newManager", "type": "address" } ], "name": "ManagerChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldAdmin", "type": "address" }, { "indexed": false, "name": "newAdmin", "type": "address" } ], "name": "AdminRightsTransferred", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" } ], "name": "MemberRemoved", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "propertyIndex", "type": "uint256" }, { "indexed": false, "name": "newValue", "type": "uint256" } ], "name": "MemberUintPropertyChanged", "type": "event" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_linkedInProfile", "type": "string" } ], "name": "setMemberLinkedInProfile", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_name", "type": "string" } ], "name": "setMemberName", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newAdmin", "type": "address" } ], "name": "transferAdminRights", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "constant": false, "inputs": [ { "name": "_target", "type": "address" } ], "name": "upgrade", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "upgradeState", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "admin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "currentImplementation", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "manager", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "memberGroupOf", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "memberIds", "outputs": [ { "name": "numbers", "type": "bytes32[]" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "memberInfo", "outputs": [ { "name": "registrationTime", "type": "uint256" }, { "name": "name", "type": "string" }, { "name": "groupId", "type": "uint256" }, { "name": "country", "type": "string" }, { "name": "linkedInProfile", "type": "string" }, { "name": "avatarUrl", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "pendingAdmin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "version", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ];

	var localWeb3 = new Web3(web3.currentProvider);

	var High1000Contract = localWeb3.eth.contract(contractAbi);
	var high1000Contract = High1000Contract.at(contractAddress);

	var createRequestCallback = function(callback) {

		return function(error, result) {

			if(error) {

				console.error(error);
				callback(null);
				return;
			}

			console.log(result);
			callback(result);
		};
	};

	/**
	 * The function returns an array of members.
	 * Each item of the list is an object that has the following properties:
	 * 1) memberId
	 * 2) registrationTime
	 * 3) name
	 * 4) groupId
	 * 5) country
	 * 6) linkedInProfile
	 * 7) avatarUrl
	 */
	this.getMembers = function(callback) {

		function buildMemberList(memberAddedEventItems) {

			var members = [];

			for(var i in memberAddedEventItems) {

				var item = memberAddedEventItems[i];

				members.push({
					memberId: item.args.memberId,
					registrationTime: item.args.registrationTime.toNumber(),
					name: item.args.name,
					groupId: item.args.groupId.toNumber(),
					country: item.args.country,
					linkedInProfile: item.args.linkedInProfile,
					avatarUrl: item.args.avatarUrl,
				});
			}

			return members;
		}

		high1000Contract.MemberAdded({}, { fromBlock: 0, toBlock: 'latest' }).get((error, eventResult) => {

			if (error) {

				console.log('Error in MemberAdded event handler: ' + error);
				callback(null);
			}
			else {
				console.log(eventResult);
				callback(buildMemberList(eventResult));
			}
		});
	};
}

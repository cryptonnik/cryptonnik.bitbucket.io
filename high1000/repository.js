function High1000Repository(useWeb3, membersJsonPath, photosDir) {

	var high1000Contract = null;

	if(useWeb3) {

		try {
			var web3 = new Web3(
				new Web3.providers.WebsocketProvider("wss://rinkeby.infura.io/ws")
			);

			var contractAddress = '0xd593f12c27c0f01be7940f50d4d31dadbd97a1e4';
			var contractAbi = [ { "constant": false, "inputs": [ { "name": "_name", "type": "string" }, { "name": "_groupId", "type": "uint8" }, { "name": "_registrationTime", "type": "uint256" }, { "name": "_country", "type": "string" }, { "name": "_linkedInProfile", "type": "string" } ], "name": "addMember", "outputs": [ { "name": "", "type": "bytes32" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "claimAdminRights", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "deleteMember", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newManager", "type": "address" } ], "name": "setManager", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_country", "type": "string" } ], "name": "setMemberCountry", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_groupId", "type": "uint8" } ], "name": "setMemberGroup", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_linkedInProfile", "type": "string" } ], "name": "setMemberLinkedInProfile", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_name", "type": "string" } ], "name": "setMemberName", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_registrationTime", "type": "uint256" } ], "name": "setMemberRegistrationTime", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newAdmin", "type": "address" } ], "name": "transferAdminRights", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_target", "type": "address" } ], "name": "upgrade", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "upgradeState", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldManager", "type": "address" }, { "indexed": false, "name": "newManager", "type": "address" } ], "name": "ManagerChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "name", "type": "string" }, { "indexed": false, "name": "groupId", "type": "uint8" }, { "indexed": false, "name": "registrationTime", "type": "uint256" }, { "indexed": false, "name": "country", "type": "string" }, { "indexed": false, "name": "linkedInProfile", "type": "string" } ], "name": "MemberAdded", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "propertyIndex", "type": "uint256" }, { "indexed": false, "name": "newValue", "type": "string" } ], "name": "MemberStringPropertyChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "propertyIndex", "type": "uint256" }, { "indexed": false, "name": "newValue", "type": "uint256" } ], "name": "MemberUintPropertyChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" } ], "name": "MemberRemoved", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldAdmin", "type": "address" }, { "indexed": false, "name": "newAdmin", "type": "address" } ], "name": "AdminRightsTransferred", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldTarget", "type": "address" }, { "indexed": false, "name": "newTarget", "type": "address" } ], "name": "ContractUpgraded", "type": "event" }, { "constant": true, "inputs": [], "name": "admin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "currentImplementation", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "manager", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "memberGroupOf", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "memberIds", "outputs": [ { "name": "numbers", "type": "bytes32[]" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "memberInfo", "outputs": [ { "name": "name", "type": "string" }, { "name": "groupId", "type": "uint256" }, { "name": "registrationTime", "type": "uint256" }, { "name": "country", "type": "string" }, { "name": "linkedInProfile", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "pendingAdmin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "version", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ];

			high1000Contract = new web3.eth.Contract(contractAbi, contractAddress);

		} catch(error) {

			console.error(error);
		}
	}

	var high1000Loader = new High1000Loader(high1000Contract, membersJsonPath, photosDir);

	this.loadMembers = function(callback) {
		high1000Loader.loadMembers(callback);
	};
}

function High1000Loader(high1000Contract, membersJsonPath, photosDir) {

	/*
	var createRequestCallback = function(callback) {

		return function(error, result) {

			if(error) {

				console.error(error);
				callback(null);
				return;
			}

			console.log(result);
			callback(result);
		};
	};
	*/

	var orderArray = ["Gabriel Brack","Simon Cocking","Amarpreet Singh","Dmitry Machikhin","Ibrahim Halkano","Maxim Vihorev","Roberto Medrano","Vladislav Siganevic","Mike Blackwood","Sydney Ifergan","Gaurav Chakraverti","Aravinda Babu","Danielle Azzaro","Francis Walugembe","Jacob Ortiz","Jan Kulisek","Gordon Jones","Giacomo Arcaro","Giovanni Casagrande","Giovanni Maranetto","Alex Litvinovich","Alexander Uglov","Christopher Butler","Amir Student","Bonnie Normile","Boris Otonicar","Jorge Rorriguez","Kaori Morikawa","Lior Zaks","Malcolm Tan","Maximilian Kobernik","Mihai Bisnel","Mofassair Hossain","Mohamed Shoieb","Roderick Warren","Renato Almedia","Sanem Avcil","Savio Gomez","Shebin John","Stanislav Sokolovsky","Thomas Enechi","Mauro Andriotto","Guillaume Micouin","Jose Merino","Kamal Mustafa","Krishnendu Chatterjee","Luciano Canzanella","Marco Rosso","Mario Pardos","Nicola Davolio","Nick Nichlos","Nithin Reddy","Nyankomo Marwa","Paresh Masani","Purushotham Maralappa","Quentin Hebrecht","Qusai Sharef","Ramy AlDamati","Ravi Ramharak","Roman Karimov","Samson Li","Sindhu Bhaskar","Stephan De Haes","Steven Martins","Tim Mak","Timo Trippler","Michael Pickens","Dan Litwak","Bryan Cutter","Alvaro Rivero","Abiodun Ayorinde","Adam Little","Antonio Caspari","Antoshi Popov","Antoun Toubia","Arthur Zubkoff","Ashutosh Bhatt","Asim Hasanov","Federico Vigano","Genevieve Leveille","Igor Marcos","Ilya Orlov","Janis Pranevics","Jason Meng","JC Awe","Jeremy Khoo","Joey Jones","William Quek","Yao Min Ng","Toomas Allmere","Vladimir Malakchi","Shinichi Abe","Bas Geelen","Cem Ozargun","Hieu tran","Radi Sajad","Guillaume Micouin","Juan Aguero","Saurabh Nandoskar","Mudaser Iqbal","Eloisa Marchesoni","Abhijith Naraparaju","Alexandre Orfevre","Ronny Lee","Manav Kalra","Gianluca Guerra","Vladimir Denis","David Tacer"];

	function getPhotoUrl(member) {
		return photosDir + member.name + ".jpg";
	}

	function orderMembers(members) {

		members.sort(function(a, b) {

			var indexA = orderArray.indexOf(a.name);
			var indexB = orderArray.indexOf(b.name);

			if(indexA != -1 && indexB != -1)
				return indexA < indexB ? -1 : 1;

			if(indexA == -1 && indexB != -1)
				return 1;

			if(indexA != -1 && indexB == -1)
				return -1;

			return ('' + a.attr).localeCompare(b.attr);
		});

		return members;
	}

	function loadMembersFromJson(callback) {

		$.ajax({
			dataType: "json",
			url: membersJsonPath,
			mimeType: "application/json",
			success: function(result) {

				for(var i = 0; i < result.length; i++) {

					result[i].photoUrl = getPhotoUrl(result[i]);
				}

				callback(orderMembers(result));
			},
			error: function(jqxhr, textStatus, error) {
				callback(null);
			}
		});
	}

	function buildMemberList(
			memberAddedEvents,
			memberRemovedEvents,
			stringPropertyChangedEvents,
			uintPropertyChangedEvents)
	{
		const membersMap = {};

		for(var i = 0; i < memberAddedEvents.length; i++) {

			var event = memberAddedEvents[i];

			membersMap[event.returnValues.memberId] = {
					properties: event.returnValues,
					blockNumber: event.blockNumber
			};
		}

		for(var i = 0; i < memberRemovedEvents.length; i++) {

			delete membersMap[memberRemovedEvents[i].returnValues.memberId];
		}

		const propertyMap = {
			1: "name",
			2: "country",
			3: "linkedInProfile",
			1001: "groupId",
			1002: "registrationTime",
		}

		function updateMembers(events) {

			for(var i = 0; i < events.length; i++) {

				const event = events[i];
				const memberState = membersMap[event.returnValues.memberId];

				if(typeof memberState === "undefined")
					continue;

				if(memberState.blockNumber > event.blockNumber)
					continue;

				memberState.properties[propertyMap[event.returnValues.propertyIndex]] = event.returnValues.newValue;
				memberState.blockNumber = event.blockNumber
			}
		}

		updateMembers(stringPropertyChangedEvents);
		updateMembers(uintPropertyChangedEvents);

		var members = [];

		for(var memberId in membersMap) {

			const memberProperties = membersMap[memberId].properties;

			members.push({
				memberId: memberProperties.memberId,
				name: memberProperties.name,
				groupId: parseInt(memberProperties.groupId),
				registrationTime: parseInt(memberProperties.registrationTime),
				country: memberProperties.country,
				linkedInProfile: memberProperties.linkedInProfile,
				photoUrl: getPhotoUrl(memberProperties)
			});
		}

		return members;
	}

	/**
	 * The function returns an array of members.
	 * Each item of the list is an object that has the following properties:
	 * 1) memberId
	 * 2) registrationTime
	 * 3) name
	 * 4) groupId
	 * 5) country
	 * 6) linkedInProfile
	 */
	this.loadMembers = function(callback) {

		if(high1000Contract === null) {

			loadMembersFromJson(callback);
			return;
		}

		function loadAllEvents(eventName, callback) {

			high1000Contract.getPastEvents(
				eventName,
				{ fromBlock: 0 },
				function (error, events) {

					if (error) {

						console.log('Error in ' + eventName + ' event handler: ' + error);
						callback(null);
					}
					else {
						console.log(events);
						callback(events);
					}
				});
		}

		loadAllEvents("MemberAdded", function(memberAddedEvents) {

			if(memberAddedEvents === null) {

				//callback(null);
				loadMembersFromJson(callback);
				return;
			}

			loadAllEvents("MemberRemoved", function(memberRemovedEvents) {

				if(memberRemovedEvents === null) {

					//callback(null);
					loadMembersFromJson(callback);
					return;
				}

				loadAllEvents("MemberStringPropertyChanged", function(stringPropertyChangedEvents) {

					if(stringPropertyChangedEvents === null) {

						//callback(null);
						loadMembersFromJson(callback);
						return;
					}

					loadAllEvents("MemberUintPropertyChanged", function(uintPropertyChangedEvents) {

						if(uintPropertyChangedEvents === null) {

						//callback(null);
						loadMembersFromJson(callback);
							return;
						}

						callback(buildMemberList(
								memberAddedEvents,
								memberRemovedEvents,
								stringPropertyChangedEvents,
								uintPropertyChangedEvents));
					});
				});
			});
		});
	};
}

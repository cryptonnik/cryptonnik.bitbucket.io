
$(document).ready(function() {

	var high1000 = new High1000Repository(false, "/high1000/members.json", "/high1000/photos/");

	$("#loadAllMembersButton").click(function() { loadAllMembers(high1000); });
});

function loadAllMembers(high1000) {

	$("#allMemberList").html("...");

	/**
	 * The function returns an array of members.
	 * Each item of the list is an object that has the following properties:
	 * 1) memberId
	 * 2) registrationTime
	 * 3) name
	 * 4) groupId
	 * 5) country
	 * 6) linkedInProfile
	 */
	high1000.loadMembers(function(members) {

		if(members === null) {

			console.error("Error occurred");
			$("#allMemberList").html("Error occurred");
			return;
		}

		//console.log(JSON.stringify(members));
		$("#membersJson").html(JSON.stringify(members));

		var membersString = "";

		for(var i = 0; i < members.length; i++) {

			if(membersString.length != 0)
				membersString += "<br /> <br />";

			var member = members[i];

			//membersString += '<img src="/high1000/photos/' + member.name +'.jpg" /><br />';
			membersString += '<img src="' + member.photoUrl + '" /><br />';

			membersString +=
				member.memberId + "<br />" +
				member.name + "<br />" +
				timeConverter(member.registrationTime) + "<br />" +
				getGroupName(member.groupId) + "<br />" +
				member.country + "<br />" +
				member.linkedInProfile;
		}

		if(membersString.length == 0)
			membersString = "No members";

		$("#allMemberList").html(membersString);
	});
}

function getGroupName(groupId) {

	switch(groupId) {

			case 1:
				return "Evangelist";

			case 2:
				return "1-100";

			case 3:
				return "101-300";

			case 4:
				return "301-1000";

			default:
				return "unknown";
		}
}

function timeConverter(UNIX_timestamp){

	var a = new Date(UNIX_timestamp * 1000);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year /*+ ', ' + ("0" + hour).substr(-2) + ':' + ("0" + min).substr(-2) + ':' + ("0" + sec).substr(-2)*/;
	return time;
}

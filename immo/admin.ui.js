
$(document).ready(function() {
		
	const PROPERTY_NAME = 1;
	const PROPERTY_COUNTRY = 2;
	const PROPERTY_LINKEDIN = 3;
	const PROPERTY_FACEBOOK = 4;
	const PROPERTY_TELEGRAM = 5;
	
	$("#newMemberRegistrationTime").datepicker();
	$("#updateMember_registrationTime").datepicker();
	
	window.high1000 = new High1000(new Web3(window.web3.currentProvider));
	
	$("#updateMembersButton").click(function() { updateMemberList(window.high1000); });
	
	$("#loadAllMembersButton").click(function() { loadAllMembers(window.high1000); });
	
	$("#memberInfoLoadButton").click(function() { 
	
		var memberId = $("#memberInfoMemberId").val().trim();
		loadMemberInfo(window.high1000, memberId); 
	});	
	
	$("#newMemberAddButton").click(function() { 
	
		var name = $("#newMemberName").val();
		var groupId = $("#newMemberGroupId").val();
		var registrationTime = $("#newMemberRegistrationTime").val();
		var country = $("#newMemberCountry").val();
		var linkedIn = $("#newMemberLinkedIn").val();
		//var avatarUrl = $("#newMemberAvatarUrl").val();
		
		registrationTime = Date.parse(registrationTime);
		
		if(isNaN(registrationTime))
			return;
		
		//addMember(window.high1000, name, groupId, country, linkedIn); 
		window.high1000.addMember(name, groupId, registrationTime / 1000, country, linkedIn, defaultTransactionCallback);
	});
	
	$("#updateMemberSetNameButton").click(function() { 
	
		var memberId = $("#updateMemberId").val().trim();
		var newName = $("#updateMemberName").val();
		
		window.high1000.setMemberName(memberId, newName, defaultTransactionCallback);
	});
	
	$("#updateMemberSetGroupButton").click(function() { 
	
		var memberId = $("#updateMemberId").val().trim();
		var newGroupId = $("#updateMemberGroupId").val();
		
		window.high1000.setMemberGroup(memberId, newGroupId, defaultTransactionCallback);
	});
	
	$("#updateMember_setRegistrationTimeButton").click(function() { 
	
		var memberId = $("#updateMemberId").val().trim();
		var registrationTime = $("#updateMember_registrationTime").val();
		
		registrationTime = Date.parse(registrationTime);
		
		if(isNaN(registrationTime))
			return;
		
		window.high1000.setMemberRegistrationTime(memberId, registrationTime / 1000, defaultTransactionCallback);
	});
	
	$("#updateMemberSetCountryButton").click(function() { 
	
		var memberId = $("#updateMemberId").val().trim();
		var newCountry = $("#updateMemberCountry").val();
		
		window.high1000.setMemberCountry(memberId, newCountry, defaultTransactionCallback);
	});
	
	$("#updateMemberSetLinkedInButton").click(function() { 
	
		var memberId = $("#updateMemberId").val().trim();
		var newLinkedIn = $("#updateMemberLinkedIn").val();
		
		window.high1000.setMemberLinkedInProfile(memberId, newLinkedIn, defaultTransactionCallback);
	});
	
	$("#updateMemberSetAvatarButton").click(function() { 
	
		var memberId = $("#updateMemberId").val().trim();
		var avatarUrl = $("#updateMemberAvatarUrl").val();
		
		window.high1000.setMemberAvatarUrl(memberId, avatarUrl, defaultTransactionCallback);
	});
		
	$("#deleteMemberButton").click(function() { 
	
		var memberId = $("#deleteMemberId").val().trim();	
		window.high1000.deleteMember(memberId, defaultTransactionCallback);
	});
});

function updateMemberList(high1000) {
	
	high1000.getMemberIds(function(memberIds) {
	
		if(memberIds === null) {
		
			console.error("Error occurred");
			return;
		}
		
		var membersString = "";
		
		for(var i = 0; i < memberIds.length; i++) {
			
			if(membersString.length != 0)
				membersString += "<br />";
			
			membersString += memberIds[i];
		}
		
		if(membersString.length == 0)
			membersString = "No members";
		
		$("#memberNumbers").html(membersString);
	});
}

function loadAllMembers(high1000) {
	
	high1000.getMembers(function(members) { 
		
		if(members === null) {
			
			console.error("Error occurred");
			return;
		}		
		
		var membersString = "";
		
		for(var i = 0; i < members.length; i++) {
			
			if(membersString.length != 0)
				membersString += "<br />";
			
			var member = members[i];
			
			membersString += 
				member.name + ", " +
				getGroupName(member.groupId) + ", " +
				timeConverter(member.registrationTime) + ", " +
				member.country + ", " +
				member.linkedInProfile + ", " +
				member.avatarUrl;
		}
		
		if(membersString.length == 0)
			membersString = "No members";
		
		$("#allMemberList").html(membersString);		
	}); 
}

function getGroupName(groupId) {
	
	switch(groupId) {
			
			case 1:
				return "Evangelist";
			
			case 2:
				return "1-100";
			
			case 3:
				return "101-300";
			
			case 4:
				return "301-1000";
				
			default:
				return "unknown";			
		}
}

function loadMemberInfo(high1000, memberId) {
	
	high1000.getMemberInfo(memberId, function(memberInfo) {
	
		if(memberInfo === null) {
		
			console.error("Error occurred");
			alert("No member with id " + memberId);
			return;
		}
		
		$("#memberInfoRegTime").html(timeConverter(memberInfo.registrationTime));
		$("#memberInfoName").html(memberInfo.name);		
		$("#memberInfoGroup").html(getGroupName(memberInfo.groupId));		
		$("#memberInfoCountry").html(memberInfo.country);
		$("#memberInfoLinkedIn").html(memberInfo.linkedInProfile);
		$("#memberInfoAvatarUrl").html(memberInfo.avatarUrl);
	});	
}

var defaultTransactionCallback = function(success) {
			
	if(!success) {
	
		alert("Error occurred");
		return;
	}
	
	alert("The transaction confirmed");
};

function timeConverter(UNIX_timestamp){
	
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year /*+ ', ' + ("0" + hour).substr(-2) + ':' + ("0" + min).substr(-2) + ':' + ("0" + sec).substr(-2) */;
  return time;
}

/*
function addMember(high1000, name, groupId, country, linkedIn, avatarUrl) {
	
	high1000.addMember(name, groupId, country, linkedIn, avatarUrl, defaultTransactionCallback);
}

function updateMemberStringProperty(high1000, memberNumber, propertyIndex, value) {
	
	high1000.updateMemberStringProperty(memberNumber, propertyIndex, value, defaultTransactionCallback);
}

function updateMemberInfo(high1000, memberNumber, info) {
	
	high1000.updateMemberData(memberNumber, false, "", true, info, function(success) {
			
		 if(!success) {
		
			alert("Error occurred");
			return;
		}
		
		alert("The transaction confirmed");
	});
}

function updateMemberNumber(high1000, number, newNumber) {
	
	high1000.updateMemberNumber(number, newNumber, function(success) {
			
		 if(!success) {
		
			alert("Error occurred");
			return;
		}
		
		alert("The transaction confirmed");
	});
}

function deleteMember(high1000, memberId) {
	
	high1000.deleteMember(memberId, defaultTransactionCallback);
}
*/
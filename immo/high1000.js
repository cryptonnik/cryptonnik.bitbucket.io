
function High1000(web3) {

	var contractAddress = '0xd593f12c27c0f01be7940f50d4d31dadbd97a1e4';
	var contractAbi = [ { "constant": false, "inputs": [ { "name": "_name", "type": "string" }, { "name": "_groupId", "type": "uint8" }, { "name": "_registrationTime", "type": "uint256" }, { "name": "_country", "type": "string" }, { "name": "_linkedInProfile", "type": "string" } ], "name": "addMember", "outputs": [ { "name": "", "type": "bytes32" } ], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "claimAdminRights", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "deleteMember", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newManager", "type": "address" } ], "name": "setManager", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_country", "type": "string" } ], "name": "setMemberCountry", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_groupId", "type": "uint8" } ], "name": "setMemberGroup", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_linkedInProfile", "type": "string" } ], "name": "setMemberLinkedInProfile", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_name", "type": "string" } ], "name": "setMemberName", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_memberId", "type": "bytes32" }, { "name": "_registrationTime", "type": "uint256" } ], "name": "setMemberRegistrationTime", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newAdmin", "type": "address" } ], "name": "transferAdminRights", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_target", "type": "address" } ], "name": "upgrade", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [], "name": "upgradeState", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldManager", "type": "address" }, { "indexed": false, "name": "newManager", "type": "address" } ], "name": "ManagerChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "name", "type": "string" }, { "indexed": false, "name": "groupId", "type": "uint8" }, { "indexed": false, "name": "registrationTime", "type": "uint256" }, { "indexed": false, "name": "country", "type": "string" }, { "indexed": false, "name": "linkedInProfile", "type": "string" } ], "name": "MemberAdded", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "propertyIndex", "type": "uint256" }, { "indexed": false, "name": "newValue", "type": "string" } ], "name": "MemberStringPropertyChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" }, { "indexed": false, "name": "propertyIndex", "type": "uint256" }, { "indexed": false, "name": "newValue", "type": "uint256" } ], "name": "MemberUintPropertyChanged", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "memberId", "type": "bytes32" } ], "name": "MemberRemoved", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldAdmin", "type": "address" }, { "indexed": false, "name": "newAdmin", "type": "address" } ], "name": "AdminRightsTransferred", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "oldTarget", "type": "address" }, { "indexed": false, "name": "newTarget", "type": "address" } ], "name": "ContractUpgraded", "type": "event" }, { "constant": true, "inputs": [], "name": "admin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "currentImplementation", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "manager", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "memberGroupOf", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "memberIds", "outputs": [ { "name": "numbers", "type": "bytes32[]" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_memberId", "type": "bytes32" } ], "name": "memberInfo", "outputs": [ { "name": "name", "type": "string" }, { "name": "groupId", "type": "uint256" }, { "name": "registrationTime", "type": "uint256" }, { "name": "country", "type": "string" }, { "name": "linkedInProfile", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "pendingAdmin", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "version", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" } ];
			
	var localWeb3 = new Web3(web3.currentProvider);
	
	var High1000Contract = localWeb3.eth.contract(contractAbi);
	var high1000Contract = High1000Contract.at(contractAddress);
		
	var createRequestCallback = function(callback) {
		
		return function(error, result) {
			
			if(error) {
			
				console.error(error);
				callback(null);
				return;
			}
			
			console.log(result);
			callback(result);
		};
	};
	
	var waitForMinedTransaction = function(txHash, interval, callback) {
	
		const transactionReceiptAsync = function() {
			
			web3.eth.getTransactionReceipt(txHash, (error, receipt) => {
				
				if (error || receipt == null) {
					
					setTimeout(transactionReceiptAsync, interval);
						
				} else {
					callback(receipt.status === "0x1");
				}
			});
		};
		
		transactionReceiptAsync();
	}
	
	var createTransactionCallback = function(callback) {
		
		return function(error, result) {
			
			if(error) {
			
				console.error(error);
				callback(false);
				return;
			}
			
			console.log(result);
			
			waitForMinedTransaction(result, 2000, function(success) {					
				callback(success);
			});
		};
	};
	
	
	this.addMember = function(name, groupId, registrationTime, country, linkedInProfile, callback) {			
		high1000Contract.addMember(name, groupId, registrationTime, country, linkedInProfile, createTransactionCallback(callback));
	};
	
	this.setMemberName = function(memberId, name, callback) {			
		high1000Contract.setMemberName(memberId, name, createTransactionCallback(callback));
	};
	
	this.setMemberCountry = function(memberId, country, callback) {			
		high1000Contract.setMemberCountry(memberId, country, createTransactionCallback(callback));
	};
	
	this.setMemberLinkedInProfile = function(memberId, linkedInProfile, callback) {			
		high1000Contract.setMemberLinkedInProfile(memberId, linkedInProfile, createTransactionCallback(callback));
	};
	
	this.setMemberAvatarUrl = function(memberId, avatarUrl, callback) {			
		high1000Contract.setAvatarUrl(memberId, avatarUrl, createTransactionCallback(callback));
	};
	
	this.setMemberGroup = function(memberId, groupId, callback) {			
		high1000Contract.setMemberGroup(memberId, groupId, createTransactionCallback(callback));
	};
	
	this.setMemberRegistrationTime = function(memberId, registrationTime, callback) {			
		high1000Contract.setMemberRegistrationTime(memberId, registrationTime, createTransactionCallback(callback));
	};
	
	this.deleteMember = function(memberId, callback) {			
		high1000Contract.deleteMember(memberId, createTransactionCallback(callback));
	};
	
	
	this.getMemberIds = function(callback) {		
		high1000Contract.memberIds(createRequestCallback(callback));
	};
	
	this.getMemberInfo = function(memberId, callback) {	
	
		high1000Contract.memberInfo(memberId, createRequestCallback(function(result) {
			
			if(result === null) {
				
				callback(null);
				return;
			}
			
			callback({
				memberId: memberId,
				name: result[0],
				groupId: result[1].toNumber(),
				registrationTime: result[2].toNumber(),
				country: result[3],
				linkedInProfile: result[4],
				avatarUrl: result[5],
			});
		}));
	};
	
	this.getMembers = function(callback) {
		
		function buildMemberList(memberAddedEventItems) {
			
			var members = [];
			
			for(var i in memberAddedEventItems) {
				
				var item = memberAddedEventItems[i];
				
				members.push({
					memberId: item.args.memberId,
					registrationTime: item.args.registrationTime.toNumber(),
					name: item.args.name,
					groupId: item.args.groupId.toNumber(),
					country: item.args.country,
					linkedInProfile: item.args.linkedInProfile,
					avatarUrl: item.args.avatarUrl,
				});
			}
			
			return members;
		}
		
		high1000Contract.MemberAdded({}, { fromBlock: 0, toBlock: 'latest' }).get((error, eventResult) => {
			
			if (error) {
				
				console.log('Error in MemberAdded event handler: ' + error);
				callback(null);
			}
			else {
				console.log(eventResult);
				
				callback(buildMemberList(eventResult));
			}
		});
	};
}
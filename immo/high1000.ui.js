
$(document).ready(function() {

	window.high1000 = new High1000(new Web3(window.web3.currentProvider));

	$("#addNextMemberButton").click(function() { addNextMember(window.high1000); });

	$("#generateOrderArrayButton").click(function() { generateOrderArray(); });
});

function addNextMember(high1000) {

	var memberListString = $("#memberList").val();

	if(memberListString.length == 0)
		return;

	var index = memberListString.indexOf("\n");

	if(index == -1)
		index = memberListString.length;

	const line = memberListString.substring(0, index);

	var parts = line.split("\t");

	if(parts.length != 5)
		return;

	var name = parts[0].trim();
	var groupId = getGroupId(parts[2].trim());
	var registrationTime = getUnixTime(parts[3].trim());
	var country = parts[1].trim();
	var linkedInProfile = parts[4].trim();

	if(groupId == -1)
		return;

	high1000.addMember(name, groupId, registrationTime, country, linkedInProfile, function(success) {

		if(!success) {

			var newValue = $("#failedTransactions").val();

			if(newValue.length > 0)
				newValue += "\n";

			newValue += line;

			$("#failedTransactions").val(newValue);
			return;
		}
	});

	$("#memberList").val(memberListString.substring(index + 1));
}

var defaultTransactionCallback = function(success) {

	if(!success) {

		alert("Error occurred");
		return;
	}

	//alert("The transaction confirmed");
};

function getUnixTime(date) {

	var parts = date.split("/");
	return Date.parse(parts[1] + "/" + parts[0] + "/" + parts[2]) / 1000;
}

function getGroupId(groupName) {

	switch(groupName) {

		case "Evangelist":
			return 1;

		case "1-100":
			return 2;

		case "101-300":
			return 3;

		case "301-1000":
			return 4;
	}

	return -1;
}

function generateOrderArray() {

	var memberListString = $("#memberList").val();
	var memberList = memberListString.split("\n");

	var members = [];

	for(var i = 0; i < memberList.length; i++) {

		var memberName = memberList[i].trim();

		if(memberName.length == 0)
			continue;

		members.push(memberName);
	}

	$("#memberOrderArray").val(JSON.stringify(members));
}

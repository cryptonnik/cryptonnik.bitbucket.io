
function High1000(web3) {

	var contractAddress = '0xbef80b1d127c92e46179c989c9bcb65cce9cc2d3';
	var contractAbi = [ { "constant": false, "inputs": [ { "name": "_target", "type": "address" } ], "name": "upgrade", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_number", "type": "uint256" }, { "name": "_name", "type": "string" } ], "name": "addMember", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_number", "type": "uint256" }, { "name": "_propertyIndex", "type": "uint256" }, { "name": "value", "type": "string" } ], "name": "updateMemberStringProperty", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "manager", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "claimOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "version", "outputs": [ { "name": "", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "initialize", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "activeMemberNumbers", "outputs": [ { "name": "numbers", "type": "uint256[]" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_number", "type": "uint256" } ], "name": "deleteMember", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "owner", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalMemberSlotsNumber", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "emptyMemberSlotsNumber", "outputs": [ { "name": "", "type": "uint256" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newManager", "type": "address" } ], "name": "setManager", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "currentImplementation", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [ { "name": "_number", "type": "uint256" } ], "name": "memberProperties", "outputs": [ { "name": "name", "type": "string" }, { "name": "country", "type": "string" }, { "name": "linkedIn", "type": "string" }, { "name": "facebook", "type": "string" }, { "name": "telegram", "type": "string" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "pendingOwner", "outputs": [ { "name": "", "type": "address" } ], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [ { "name": "_number", "type": "uint256" }, { "name": "_newNumber", "type": "uint256" } ], "name": "updateMemberNumber", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": false, "inputs": [ { "name": "_newOwner", "type": "address" } ], "name": "transferOwnership", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [ { "indexed": true, "name": "previousOwner", "type": "address" }, { "indexed": true, "name": "newOwner", "type": "address" } ], "name": "OwnershipTransferred", "type": "event" }, { "anonymous": false, "inputs": [ { "indexed": false, "name": "previousTarget", "type": "address" }, { "indexed": false, "name": "newTarget", "type": "address" } ], "name": "ContractUpgraded", "type": "event" } ];
	
	var localWeb3 = new Web3(web3.currentProvider);
	
	var High1000Contract = localWeb3.eth.contract(contractAbi);
	var high1000Contract = High1000Contract.at(contractAddress);
		
	var createRequestCallback = function(callback) {
		
		return function(error, result) {
			
			if(error) {
			
				console.error(error);
				callback(null);
				return;
			}
			
			console.log(result);
			callback(result);
		};
	};
	
	var waitForMinedTransaction = function(txHash, interval, callback) {
	
		const transactionReceiptAsync = function() {
			
			web3.eth.getTransactionReceipt(txHash, (error, receipt) => {
				
				if (error || receipt == null) {
					
					setTimeout(transactionReceiptAsync, interval);
						
				} else {
					callback(receipt.status === "0x1");
				}
			});
		};
		
		transactionReceiptAsync();
	}
	
	var createTransactionCallback = function(callback) {
		
		return function(error, result) {
			
			if(error) {
			
				console.error(error);
				callback(false);
				return;
			}
			
			console.log(result);
			
			waitForMinedTransaction(result, 2000, function(success) {					
				callback(success);
			});
		};
	};
	
	this.getMemberNumbers = function(callback) {		
		high1000Contract.activeMemberNumbers(createRequestCallback(callback));
	};
	
	this.getMemberProperties = function(memberNumber, callback) {		
		high1000Contract.memberProperties(memberNumber, createRequestCallback(callback));
	};
	
	this.addMember = function(number, name, callback) {			
		high1000Contract.addMember(number, name, createTransactionCallback(callback));
	};
	
	this.updateMemberStringProperty = function(number, propertyIndex, value, callback) {			
		high1000Contract.updateMemberStringProperty(number, propertyIndex, value, createTransactionCallback(callback));
	};
	
	this.updateMemberNumber = function(number, newNumber, callback) {			
		high1000Contract.updateMemberNumber(number, newNumber, createTransactionCallback(callback));
	};
	
	this.deleteMember = function(number, callback) {			
		high1000Contract.deleteMember(number, createTransactionCallback(callback));
	};
}
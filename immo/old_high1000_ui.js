
$(document).ready(function() {
		
	const PROPERTY_NAME = 1;
	const PROPERTY_COUNTRY = 2;
	const PROPERTY_LINKEDIN = 3;
	const PROPERTY_FACEBOOK = 4;
	const PROPERTY_TELEGRAM = 5;
	
	window.high1000 = new High1000(new Web3(window.web3.currentProvider));
	
	$("#updateMembersButton").click(function() { updateMemberList(window.high1000); });	
	
	$("#loadMemberButton").click(function() { 
	
		var memberNumber = parseInt($("#loadMemberNumber").val());
		loadMemberData(window.high1000, memberNumber); 
	});	
	
	$("#addMemberButton").click(function() { 
	
		var number = parseInt($("#addMemberNumber").val());
		var name = $("#addMemberName").val();
		
		addMember(window.high1000, number, name); 
	});
	
	$("#updateMemberNameButton").click(function() { 
	
		var memberNumber = parseInt($("#updateMemberNumber").val());
		var newValue = $("#updateMemberName").val();
		
		updateMemberStringProperty(window.high1000, memberNumber, PROPERTY_NAME, newValue); 
	});
	
	$("#updateMemberCountryButton").click(function() { 
	
		var memberNumber = parseInt($("#updateMemberNumber").val());
		var newValue = $("#updateMemberCountry").val();
		
		updateMemberStringProperty(window.high1000, memberNumber, PROPERTY_COUNTRY, newValue); 
	});
	
	$("#updateMemberLinkedInButton").click(function() { 
	
		var memberNumber = parseInt($("#updateMemberNumber").val());
		var newValue = $("#updateMemberLinkedIn").val();
		
		updateMemberStringProperty(window.high1000, memberNumber, PROPERTY_LINKEDIN, newValue); 
	});
	
	$("#updateMemberFacebookButton").click(function() { 
	
		var memberNumber = parseInt($("#updateMemberNumber").val());
		var newValue = $("#updateMemberFacebook").val();
		
		updateMemberStringProperty(window.high1000, memberNumber, PROPERTY_FACEBOOK, newValue); 
	});
	
	$("#updateMemberTelegramButton").click(function() { 
	
		var memberNumber = parseInt($("#updateMemberNumber").val());
		var newValue = $("#updateMemberTelegram").val();
		
		updateMemberStringProperty(window.high1000, memberNumber, PROPERTY_TELEGRAM, newValue); 
	});
	
	$("#updateMemberNumberButton").click(function() { 
	
		var memberNumber = parseInt($("#updateMemberNumber").val());
		var newNumber = $("#updateMemberNewNumber").val();
		
		updateMemberNumber(window.high1000, memberNumber, newNumber); 
	});	
		
	$("#deleteMemberButton").click(function() { 
	
		var memberNumber = parseInt($("#deleteMemberNumber").val());		
		deleteMember(window.high1000, memberNumber); 
	});
});

function updateMemberList(high1000) {
	
	high1000.getMemberNumbers(function(members) {
	
		if(members === null) {
		
			console.error("Error occurred");
			return;
		}
		
		var membersString = "";
		
		for(var i = 0; i < members.length; i++) {
			
			if(membersString.length != 0)
				membersString += ", ";
			
			membersString += members[i];
		}
		
		if(membersString.length == 0)
			membersString = "No members";
		
		$("#memberNumbers").html(membersString);
	});
}

function loadMemberData(high1000, memberNumber) {
	
	high1000.getMemberProperties(memberNumber, function(result) {
	
		if(result === null) {
		
			console.error("Error occurred");
			alert("No member with number " + memberNumber);
			return;
		}
		
		$("#loadMemberName").html(result[0]);
		$("#loadMemberCountry").html(result[1]);
		$("#loadMemberLinkedIn").html(result[2]);
		$("#loadMemberFacebook").html(result[3]);
		$("#loadMemberTelegram").html(result[4]);
	});	
}

function addMember(high1000, number, name) {
	
	high1000.addMember(number, name, function(success) {
			
		 if(!success) {
		
			alert("Error occurred");
			return;
		}
		
		alert("The transaction confirmed");
	});
}

function updateMemberStringProperty(high1000, memberNumber, propertyIndex, value) {
	
	high1000.updateMemberStringProperty(memberNumber, propertyIndex, value, function(success) {
			
		 if(!success) {
		
			alert("Error occurred");
			return;
		}
		
		alert("The transaction confirmed");
	});
}
/*
function updateMemberInfo(high1000, memberNumber, info) {
	
	high1000.updateMemberData(memberNumber, false, "", true, info, function(success) {
			
		 if(!success) {
		
			alert("Error occurred");
			return;
		}
		
		alert("The transaction confirmed");
	});
}*/

function updateMemberNumber(high1000, number, newNumber) {
	
	high1000.updateMemberNumber(number, newNumber, function(success) {
			
		 if(!success) {
		
			alert("Error occurred");
			return;
		}
		
		alert("The transaction confirmed");
	});
}

function deleteMember(high1000, memberId) {
	
	high1000.deleteMember(memberId, function(success) {
			
		 if(!success) {
		
			alert("Error occurred");
			return;
		}
		
		alert("The transaction confirmed");
	});
}